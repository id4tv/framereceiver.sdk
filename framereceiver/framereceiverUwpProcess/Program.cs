﻿using System;

// This example code shows how you could implement the required main function for a 
// Console UWP Application. You can replace all the code inside Main with your own custom code.

// You should also change the Alias value in the AppExecutionAlias Extension in the 
// Package.appxmanifest to a value that you define. To edit this file manually, right-click
// it in Solution Explorer and select View Code, or open it with the XML Editor.

namespace framereceiverUwpProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Hello - no args");
            }
            else
            {
                for (int i = 0; i < args.Length; i++)
                {
                    Console.WriteLine($"arg[{i}] = {args[i]}");
                }
            }

            try
            {
                framereceiverUWP framereceiverUWP = new framereceiverUWP();

                framereceiverUWP.Init(@"c:\Logs\ViBoxDemo\FrameReceiver\", "http://ec2-18-197-183-162.eu-central-1.compute.amazonaws.com:20006/", "2df5efb9-34d6-4023-8f17-aad98d33c60e", 0);
            }
            catch (Exception Error)
            {
                Console.WriteLine(Error.ToString());
            }

            Console.WriteLine("Press x to quit");
            Console.ReadLine();
        }
    }
}
