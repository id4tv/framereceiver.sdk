﻿using framereceiver.uwp;
using System;

namespace framereceiver.core
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Hello - no args");
            }
            else
            {
                for (int i = 0; i < args.Length; i++)
                {
                    Console.WriteLine($"arg[{i}] = {args[i]}");
                }
            }

            try
            {
                framereceiverUWP framereceiverUWP = new framereceiverUWP();

                framereceiverUWP.Init(@"c:\Logs\ViBoxDemo\FrameReceiver\", "http://ec2-18-197-183-162.eu-central-1.compute.amazonaws.com:20006/", "2df5efb9-34d6-4023-8f17-aad98d33c60e", 0);
            }
            catch (Exception Error)
            {
                Console.WriteLine(Error.ToString());
            }

            Console.WriteLine("Press x to quit");
            Console.ReadLine();
        }
    }
}
