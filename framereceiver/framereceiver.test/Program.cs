﻿using FrameReceiverSdk;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace framereceiver.test
{
    class Program
    {
        //private static ErrorCallBackDelegate m_ErrorCallBackDelegate;
        //private static FrameCallbackDelegate m_FrameCallbackDelegate;

        //static void Main(string[] args)
        //{
        //    string m_ServerAddress = "192.168.1.181";

        //    m_ErrorCallBackDelegate = new ErrorCallBackDelegate(ErrorCallBack);
        //    m_FrameCallbackDelegate = new FrameCallbackDelegate(FrameCallback);

        //    FrameReceiverHelper.FrameReceiver_initialize(FrameReceiverSdk.Plugin.SL_MediaServer, m_ServerAddress.ToString(), @"c:\Logs\ViBoxDemo\FrameReceiver\");

        //    FrameReceiverHelper.backend_initialize(@"c:\Logs\ViBoxDemo\FrameReceiver\Dll.FrameReceiver.log", output_format.bgr32_bgrx);

        //    FrameReceiverHelper.backend_register_error_callback(m_ServerAddress.ToString(), m_ErrorCallBackDelegate);
        //    FrameReceiverHelper.backend_register_frame_callback(m_ServerAddress.ToString(), m_FrameCallbackDelegate);

        //    List<UInt32> channel = new List<UInt32>();

        //    channel.Add(0);

        //    FrameReceiverHelper.backend_register_channel(m_ServerAddress.ToString(), channel.ToArray(), (uint)channel.Count);

        //    Console.Write("\nPress X to Exit");

        //    do { } while (Console.ReadLine().ToUpper() != "X");
        //}

        //private static bool FrameCallback(string _ip, uint _id, frame_type _frame_type, IntPtr _frame, uint _frame_size, ushort _width, ushort _height)
        //{
        //    throw new NotImplementedException();
        //}

        //private static bool ErrorCallBack(string _ip, error_type _type, int error_id, string _error_str)
        //{
        //    throw new NotImplementedException();
        //}


        static void Main(string[] args)
        {
            RunProcess runProcess = new RunProcess();

            runProcess.Start();

            Task.Run(() => runProcess.TestAsync());

            Console.Write("\nPress X to Exit");

            do { } while (Console.ReadLine().ToUpper() != "X");

            runProcess.Stop();
        }


    }

    class RunProcess
    {
        Process pipeClient = null;

        public void Start()
        {
            pipeClient = new Process();
            pipeClient.StartInfo.FileName = @"framereceiveruwpapp.exe";
        }

        public async Task TestAsync()
        {
            if (pipeClient != null)
            {
                using (AnonymousPipeServerStream pipeServer = new AnonymousPipeServerStream(PipeDirection.In, HandleInheritability.Inheritable))
                {
                    pipeClient.StartInfo.Arguments = pipeServer.GetClientHandleAsString();
                    pipeClient.StartInfo.UseShellExecute = false;
                    pipeClient.Start();

                    pipeServer.DisposeLocalCopyOfClientHandle();

                    using (StreamReader sr = new StreamReader(pipeServer))
                    {
                        string data = null;
                        do
                        {
                            data = await sr.ReadLineAsync();

                            if(data != null)
                            {

                            }
                        }
                        while (data != null);
                    }
                }
            }
        }
        
        public void Stop()
        {
            if(pipeClient != null)
            {
                try
                {
                    pipeClient.Kill();
                }
                catch (Exception Error)
                {
                }

                pipeClient.Close();
            }
        }
    }
}
