﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameReceiverSdk
{
    public enum Plugin
    {
        Dll_FrameReceiver,
        SL_MediaServer,
    };

    public enum output_format
    {
        rgb24,
        bgr32_bgrx,
    };

    public enum frame_type
    {
        type_jpeg,
        type_rgb24,
        type_bgr32_bgrx,
        type_h264,
    };

    public enum error_type
    {
        server_close_connection,
        register_channel_failed,
        connect_failed,
        recv_failed,
        send_failed
    };
}
