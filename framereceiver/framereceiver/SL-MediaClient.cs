﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameReceiverSdk
{
    internal class SL_MediaClient : IDisposable     
    {
        #region Private Member
        private Dictionary<string, Dictionary<uint, SL_MediaPlayer>> m_Players = new Dictionary<string, Dictionary<uint, SL_MediaPlayer>>();
        private Dictionary<string, FrameCallbackDelegate> m_FrameCallbackDelegates = new Dictionary<string, FrameCallbackDelegate>();
        private Dictionary<string, ErrorCallBackDelegate> m_ErrorCallbackDelegates = new Dictionary<string, ErrorCallBackDelegate>();
        private string m_SL_ConfigServer_Address;
        private string m_SL_MediaServer_Address;
        private Dictionary<string, string> m_BackendID = new Dictionary<string, string>();
        #endregion

        #region Constructor
        public SL_MediaClient()
        {
        } 
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SL_MediaClient() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        #region Internal Methods

        internal bool DisconnectServer(string _Ip)
        {
            try
            {
                if(m_Players.ContainsKey(_Ip))
                {
                    foreach (var item in m_Players[_Ip])
                    {
                        item.Value.Dispose();
                    }
                }
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }

            return true;
        }

        internal void GetDebuginfo(string _Ip, ref debug_info _DebugInfo)
        {
            try
            {
                _DebugInfo = new debug_info();
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }
        }

        internal bool ConnectServer(ref ConnectParameter _ConnectParameter)
        {
            try
            {
                m_SL_ConfigServer_Address = System.Text.UTF8Encoding.UTF8.GetString(_ConnectParameter._Plugin_parameter._web_server_address, 0, _ConnectParameter._Plugin_parameter._web_server_address.Count()).TrimEnd('\0');

                FrameReceiverHelper.s_Log.Debug("SL_MediaClient : ConnectServer "
                    + "WebServerAddress" + " : " + m_SL_ConfigServer_Address.ToString());

                if (!m_Players.ContainsKey(_ConnectParameter.Ip))
                {
                    m_Players.Add(_ConnectParameter.Ip, new Dictionary<uint, SL_MediaPlayer>());
                }
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }

            return true;
        }

        internal void registerFrameCallback(string _Ip, FrameCallbackDelegate _FrameCallback)
        {
            try
            {
                if(m_FrameCallbackDelegates.ContainsKey(_Ip))
                {
                    m_FrameCallbackDelegates[_Ip] = _FrameCallback;

                    if(m_Players.ContainsKey(_Ip))
                    {
                        foreach (var item in m_Players[_Ip])
                        {
                            item.Value.FrameCallback = _FrameCallback;
                        }
                    }
                }
                else
                {
                    m_FrameCallbackDelegates.Add(_Ip, _FrameCallback);
                }
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }
        }

        internal void RegisterErrorCallback(string _Ip, ErrorCallBackDelegate _ErrorCallback)
        {
            try
            {
                if (m_FrameCallbackDelegates.ContainsKey(_Ip))
                {
                    m_ErrorCallbackDelegates[_Ip] = _ErrorCallback;

                    if (m_Players.ContainsKey(_Ip))
                    {
                        foreach (var item in m_Players[_Ip])
                        {
                            item.Value.ErrorCallback = _ErrorCallback;
                        }
                    }
                }
                else
                {
                    m_ErrorCallbackDelegates.Add(_Ip, _ErrorCallback);
                }
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }
        }

        internal bool RegisterChannel(string _Ip, uint[] _CnlArray)
        {
            try
            {
                if (m_Players.ContainsKey(_Ip))
                {
                    //Remove unused Cnl

                    for (int i = 0; i < m_Players[_Ip].Keys.Count(); i++)
                    {
                         uint cnl = m_Players[_Ip].ElementAt(i).Key;

                        if ((m_Players[_Ip].ContainsKey(cnl)) && (!_CnlArray.Contains(cnl)))
                        {
                            if(m_Players[_Ip][cnl] != null)
                            {
                                m_Players[_Ip][cnl].Dispose();
                            }
                            m_Players[_Ip].Remove(cnl);
                            --i;
                        }
                    }

                    //Add missing Cnl

                    foreach (var item in _CnlArray)
                    {
                        AddSL_MediaPlayer(_Ip, item);
                    }
                }
                else
                {
                    m_Players.Add(_Ip, new Dictionary<uint, SL_MediaPlayer>());

                    foreach (var item in _CnlArray)
                    {
                        AddSL_MediaPlayer(_Ip, item);
                    }
                }
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }

            return true;
        }

        internal string GetBackendId(string _Ip)
        {
            return "2df5efb9-34d6-4023-8f17-aad98d33c60e";
            //if (m_BackendID.ContainsKey(_Ip))
            //{
            //    return m_BackendID[_Ip];
            //}
            //else
            //{
            //    m_BackendID.Add(_Ip, Guid.NewGuid().ToString());
            //    return m_BackendID[_Ip];
            //}
        }

        internal string GetSL_MediaServer_Address()
        {
            return "http://ec2-3-120-140-67.eu-central-1.compute.amazonaws.com:20006/";
        }

        #endregion

        #region Private Method

        private void AddSL_MediaPlayer(string _Ip, uint _Cnl)
        {
            try
            {
                if (!m_Players[_Ip].ContainsKey(_Cnl))
                {
                    var sl_MediaPlayer = new SL_MediaPlayer(this, _Cnl, _Ip);

                    if (m_FrameCallbackDelegates.ContainsKey(_Ip))
                    {
                        sl_MediaPlayer.FrameCallback = m_FrameCallbackDelegates[_Ip];
                    }

                    if (m_ErrorCallbackDelegates.ContainsKey(_Ip))
                    {
                        sl_MediaPlayer.ErrorCallback = m_ErrorCallbackDelegates[_Ip];
                    }

                    m_Players[_Ip].Add(_Cnl, sl_MediaPlayer);
                }
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }
        }

        #endregion
    }
}
