﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FrameReceiverSdk
{
    internal static class FrameReceiverWrapper
    {
        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_initialize", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool backend_initialize([MarshalAsAttribute(UnmanagedType.LPWStr)] string _log_path, output_format _output_format);

        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_register_error_callback", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void backend_register_error_callback([MarshalAsAttribute(UnmanagedType.LPWStr)] string _ip, ErrorCallBackDelegate _error_cb);

        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_register_frame_callback", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void backend_register_frame_callback([MarshalAsAttribute(UnmanagedType.LPWStr)] string _ip, FrameCallbackDelegate _frame_cb);

        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_register_channel", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool backend_register_channel([MarshalAsAttribute(UnmanagedType.LPWStr)] string _ip, UInt32[] _cnl_array, UInt32 _cnl_array_size);

        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_connect_server", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool backend_connect_server([In, MarshalAs(UnmanagedType.Struct)] ref ConnectParameter _connectParameter);

        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_get_debug_info", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void backend_get_debug_info([MarshalAsAttribute(UnmanagedType.LPWStr)] string _ip, [In, Out, MarshalAs(UnmanagedType.Struct)] ref debug_info _debug_info);

        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_disconnect_server", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool backend_disconnect_server([MarshalAsAttribute(UnmanagedType.LPWStr)] string _ip);

        [DllImport(@"Dll.FrameReceiver.dll", EntryPoint = "backend_uninitialize", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool backend_uninitialize();
    }
}
