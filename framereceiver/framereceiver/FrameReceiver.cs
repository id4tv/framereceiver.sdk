﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FrameReceiverSdk
{
    public static class FrameReceiverHelper
    {
        internal static readonly log4net.ILog s_Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Plugin s_Plugin { get; set; }
        private static SL_MediaClient s_SL_MediaClient { get; set; }

        public static FileVersionInfo s_FileVersion { get; private set; }

        static FrameReceiverHelper()
        {
        }

        public static void FrameReceiver_initialize(Plugin _Plugin, string _ip, string _log_path)
        {
            Logger.Setup(_log_path + "FrameReceiverHelper.log");

            s_Log.Debug("FrameReceiverHelper : Receive FrameReceiver_initialize "
                + "_Plugin" + " : " + _Plugin.ToString() + " "
                + " , " + "_ip" + " : " + _ip.ToString() + " "
                + " , " + "_log_path" + " : " + _log_path.ToString());

            if ((s_Plugin == Plugin.Dll_FrameReceiver) && (_Plugin == Plugin.SL_MediaServer))
            {
                try
                {
                    //Disconnect FrameReceiverWrapper
                    FrameReceiverWrapper.backend_disconnect_server(_ip);
                }
                catch (Exception Error)
                {
                    s_Log.Error("FrameReceiverHelper", Error);
                }
            }

            if ((s_Plugin == Plugin.SL_MediaServer) && (_Plugin == Plugin.Dll_FrameReceiver))
            {
                try
                {
                    // Disconnect SL_MediaServer  
                    if(s_SL_MediaClient != null)
                    {
                        s_SL_MediaClient.Dispose();
                        s_SL_MediaClient = null;
                    }
                }
                catch (Exception Error)
                {
                    s_Log.Error("FrameReceiverHelper", Error);
                }
            }

            s_Plugin = _Plugin;
        }

        public static bool backend_initialize(string _log_path, output_format _output_format)
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_initialize "
                + "_log_path" + " : " + _log_path.ToString() + " "
                + " , " + "_output_format" + " : " + _output_format.ToString());

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                return FrameReceiverWrapper.backend_initialize(_log_path, _output_format);
            }
            else
            {
                try
                {
                    if (_output_format == output_format.bgr32_bgrx)
                    {
                        s_SL_MediaClient = new SL_MediaClient();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception Error)
                {
                    return false;
                }                
            }
        }

        public static void backend_register_error_callback(string _ip, ErrorCallBackDelegate _error_cb)
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_register_error_callback "
                + "_ip" + " : " + _ip.ToString());

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                FrameReceiverWrapper.backend_register_error_callback(_ip, _error_cb);
            }
            else
            {
                if(s_SL_MediaClient != null)
                {
                    s_SL_MediaClient.RegisterErrorCallback(_ip, _error_cb);
                }
            }
        }
        public static void backend_register_frame_callback(string _ip, FrameCallbackDelegate _frame_cb)
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_register_frame_callback "
                + "_ip" + " : " + _ip.ToString());

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                FrameReceiverWrapper.backend_register_frame_callback(_ip, _frame_cb);
            }
            else
            {
                if (s_SL_MediaClient != null)
                {
                    s_SL_MediaClient.registerFrameCallback(_ip, _frame_cb);
                }
            }
        }

        public static bool backend_register_channel(string _ip, UInt32[] _cnl_array, UInt32 _cnl_array_size)
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_register_channel "
                + "_ip" + " : " + _ip.ToString() + " "
                + " , " + "_cnl_array_size" + " : " + _cnl_array_size.ToString());

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                return FrameReceiverWrapper.backend_register_channel(_ip, _cnl_array, _cnl_array_size);
            }
            else
            {
                if (s_SL_MediaClient != null)
                {
                    return s_SL_MediaClient.RegisterChannel( _ip, _cnl_array);
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool backend_connect_server(ref ConnectParameter _connectParameter)
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_connect_server");

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                return FrameReceiverWrapper.backend_connect_server(ref _connectParameter);
            }
            else
            {
                if (s_SL_MediaClient != null)
                {
                    return s_SL_MediaClient.ConnectServer(ref _connectParameter);
                }
                else
                {
                    return false;
                }
            }
        }

        public static void backend_get_debug_info(string _ip, ref debug_info _debug_info)
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_get_debug_info "
                + "_ip" + " : " + _ip.ToString());

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                FrameReceiverWrapper.backend_get_debug_info(_ip, ref _debug_info);
            }
            else
            {
                if (s_SL_MediaClient != null)
                {
                    s_SL_MediaClient.GetDebuginfo(_ip, ref _debug_info);
                }
            }
        }

        public static bool backend_disconnect_server(string _ip)
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_get_debug_info "
                + "_ip" + " : " + _ip.ToString());

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                return FrameReceiverWrapper.backend_disconnect_server(_ip);
            }
            else
            {
                if (s_SL_MediaClient != null)
                {
                    return s_SL_MediaClient.DisconnectServer(_ip);
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool backend_uninitialize()
        {
            s_Log.Debug("FrameReceiverHelper : Receive backend_uninitialize");

            if (s_Plugin == Plugin.Dll_FrameReceiver)
            {
                return FrameReceiverWrapper.backend_uninitialize();
            }
            else
            {
                if (s_SL_MediaClient != null)
                {
                    try
                    {
                        // Disconnect SL_MediaServer  
                        if (s_SL_MediaClient != null)
                        {
                            s_SL_MediaClient.Dispose();
                            s_SL_MediaClient = null;
                        }

                        return true;
                    }
                    catch (Exception Error)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
    }

    public class Logger
    {
        public static void Setup(string _log_path)
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            PatternLayout patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = "%date [%thread] %-5level %logger - %message%newline";
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender();
            roller.AppendToFile = true;
            roller.File = _log_path;
            roller.Layout = patternLayout;
            roller.MaxSizeRollBackups = 5;
            roller.MaximumFileSize = "5MB";
            roller.RollingStyle = RollingFileAppender.RollingMode.Size;
            roller.StaticLogFileName = true;
            roller.ImmediateFlush = true;
            roller.LockingModel = new FileAppender.MinimalLock();
            roller.ActivateOptions();

            hierarchy.Root.AddAppender(roller);
            hierarchy.Root.Level = Level.All;
            hierarchy.Configured = true;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct debug_info
    {
        public UInt32 fps;
        public float dispatcher_interval;
        public float recv_interval;
        public UInt32 buffer_level;
        public UInt32 miss_frame;
        public float frame_callback_cost_time;
        public UInt32 package_size;
        public float frame_decode_time;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 255)]
        public UInt32[] cnl_list_buffer_level;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 255)]
        public float[] cnl_dispatcher_interval;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct ConnectParameter
    {
        [MarshalAsAttribute(UnmanagedType.LPWStr)]
        public string ip; //_ip format: 127.0.0.1
        public UInt16 _port;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] _timeout_ms;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] _dispatcher_interval;
        public byte _bUdp;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] _udpPackSize;
        public byte _Enable_plugin;
        public PluginParameter _Plugin_parameter;

        public string Ip { get => ip; set => ip = value; }
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct PluginParameter
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 260)]
        public byte[] _web_server_address;
    }

    internal static class EmbeddedDllClass
    {
        private static string tempFolder = "";

        /// <summary>
        /// Extract DLLs from resources to temporary folder
        /// </summary>
        /// <param name="dllName">name of DLL file to create (including dll suffix)</param>
        /// <param name="resourceBytes">The resource name (fully qualified)</param>
        internal static void ExtractEmbeddedDlls(string dllName, byte[] resourceBytes)
        {
            Assembly assem = Assembly.GetExecutingAssembly();
            string[] names = assem.GetManifestResourceNames();
            AssemblyName an = assem.GetName();

            // The temporary folder holds one or more of the temporary DLLs
            // It is made "unique" to avoid different versions of the DLL or architectures.
            tempFolder = String.Format("{0}.{1}.{2}", an.Name, an.ProcessorArchitecture, an.Version);

            string dirName = Path.Combine(Path.GetTempPath(), tempFolder);
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }

            // Add the temporary dirName to the PATH environment variable (at the head!)
            string path = Environment.GetEnvironmentVariable("PATH");
            string[] pathPieces = path.Split(';');
            bool found = false;
            foreach (string pathPiece in pathPieces)
            {
                if (pathPiece == dirName)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                Environment.SetEnvironmentVariable("PATH", dirName + ";" + path);
            }

            // See if the file exists, avoid rewriting it if not necessary
            string dllPath = Path.Combine(dirName, dllName);
            bool rewrite = true;
            if (File.Exists(dllPath))
            {
                byte[] existing = File.ReadAllBytes(dllPath);
                if (resourceBytes.SequenceEqual(existing))
                {
                    rewrite = false;
                }
            }
            if (rewrite)
            {
                File.WriteAllBytes(dllPath, resourceBytes);
            }
        }

        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern IntPtr LoadLibrary(string lpFileName);

        /// <summary>
        /// managed wrapper around LoadLibrary
        /// </summary>
        /// <param name="dllName"></param>
        static public FileVersionInfo LoadDll(string dllName)
        {
            if (tempFolder == "")
            {
                throw new Exception("Please call ExtractEmbeddedDlls before LoadDll");
            }
            IntPtr h = LoadLibrary(dllName);
            if (h == IntPtr.Zero)
            {
                Exception e = new Win32Exception();
                throw new DllNotFoundException("Unable to load library: " + dllName + " from " + tempFolder, e);
            }
            else
            {
                string dirName = Path.Combine(Path.GetTempPath(), tempFolder);
                return FileVersionInfo.GetVersionInfo(dirName + @"\" + dllName);
            }
        }

    }

    public delegate bool ErrorCallBackDelegate([MarshalAsAttribute(UnmanagedType.LPWStr)] string _ip, error_type _type, int error_id, string _error_str);

    public delegate bool FrameCallbackDelegate([MarshalAsAttribute(UnmanagedType.LPWStr)] string _ip, UInt32 _id, frame_type _frame_type, IntPtr _frame, UInt32 _frame_size, UInt16 _width, UInt16 _height);
}
