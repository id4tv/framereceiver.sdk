﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FrameReceiverSdk
{
    internal class SL_MediaPlayer : IDisposable
    {
        #region Properties
        public FrameCallbackDelegate FrameCallback { get; internal set; }
        public ErrorCallBackDelegate ErrorCallback { get; internal set; }
        public uint Channel { get; internal set; }
        public string Ip { get; internal set; }
        public string BackendId { get; internal set; }
        public string SL_MediaServer_Address { get; internal set; }
        #endregion

        #region Private Member
        private SL_MediaClient Parent;
        #endregion

        #region Constructor
        public SL_MediaPlayer(SL_MediaClient _Parent, uint _Channel, string _Ip)
        {
            FrameReceiverHelper.s_Log.Debug("SL_MediaPlayer : Constructor "
                + "_Channel" + " : " + _Channel.ToString() + " "
                + " , " + "_Ip" + " : " + _Ip.ToString());

            Parent = _Parent;
            Ip = _Ip;
            Channel = _Channel;

            Init();
        } 
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            FrameReceiverHelper.s_Log.Debug("SL_MediaPlayer : Dispose "
                + "_Channel" + " : " + Channel.ToString() + " "
                + " , " + "_Ip" + " : " + Ip.ToString());

            if (!disposedValue)
            {
                if (disposing)
                {
                    Parent = null;

                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SL_MediaPlayer() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        #region Private Member

        private void Init()
        {
            try
            {
                // Get SL-MediaServer Address
                SL_MediaServer_Address = Parent.GetSL_MediaServer_Address();

                //GetBackend GUID
                BackendId = Parent.GetBackendId(Ip);
            }
            catch (Exception Error)
            {
                FrameReceiverHelper.s_Log.Error("SL_MediaClient", Error);
            }
        }

        #endregion
    }
}
