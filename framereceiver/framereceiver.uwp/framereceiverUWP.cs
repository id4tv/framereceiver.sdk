﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MixedReality.WebRTC;
using Newtonsoft.Json;

namespace framereceiver.uwp
{
    public sealed class framereceiverUWP : IDisposable
    {
        #region Properties
        public string BackendId { get; internal set; }
        public string SL_MediaServer_Address { get; internal set; }

        public uint Channel { get; internal set; }
        #endregion

        #region Action
        public Action<string, string> ActionWriteLog = null;
        public Action<string, Exception> ActionWriteException = null;
        public Action<uint, uint, IntPtr, int> ActionFrameReceive = null;
        #endregion

        #region Private Member
        private HttpClient m_HttpClient = new HttpClient();
        private PeerConnection m_PeerConnection = new PeerConnection();
        #endregion

        #region Private Member

        public void DoWriteLog(string _module, string _log)
        {
            try
            {
                if(ActionWriteLog != null)
                {
                    ActionWriteLog(_module, _log);
                }
            }
            catch
            {
            }
        }

        public void DoWriteLog(string _module, Exception _Error)
        {
            try
            {
                if (ActionWriteException != null)
                {
                    ActionWriteException(_module, _Error);
                }
            }
            catch
            {
            }
        }

        public void Init(string _log_path, string _SL_MediaServer_Address, string _BackendId, uint _Channel)
        {
            try
            {
                SL_MediaServer_Address = _SL_MediaServer_Address;

                BackendId = _BackendId;

                Channel = _Channel;

                //Create http Connection
                m_HttpClient.BaseAddress = new Uri(SL_MediaServer_Address);
                m_HttpClient.DefaultRequestHeaders.Accept.Clear();
                m_HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                m_HttpClient.Timeout = new TimeSpan(0, 0, 30);


                //Create Offer

                Task.Run(() => CreateOffer());
            }
            catch (Exception Error)
            {
                DoWriteLog("SL_MediaClient", Error);
            }
        }

        private async Task CreateOffer()
        {
            try
            {
                if (Channel == 0)
                {
                    DoWriteLog("SL_MediaPlayer", "CreateOffer "
                        + "_Channel" + " : " + Channel.ToString() + " "
                        + " , " + "_BackendId" + " : " + BackendId.ToString());

                    await m_PeerConnection.InitializeAsync(new PeerConnectionConfiguration
                    {
                        IceServers = new List<IceServer>
                        {
                            new IceServer{ Urls = { "stun:stun.l.google.com:19302" } }
                        }
                    });

                    m_PeerConnection.VideoTrackAdded += PeerConnection_VideoTrackAdded;

                    m_PeerConnection.LocalSdpReadytoSend += PeerConnection_LocalSdpReadytoSendAsync;

                    m_PeerConnection.Connected += M_PeerConnection_Connected;

                    m_PeerConnection.RenegotiationNeeded += M_PeerConnection_RenegotiationNeeded;

                    m_PeerConnection.AddTransceiver(MediaKind.Video);

                    m_PeerConnection.CreateOffer();
                }
            }
            catch (Exception Error)
            {
                DoWriteLog("SL_MediaClient", Error);
            }
        }

        private void M_PeerConnection_RenegotiationNeeded()
        {
            try
            {
                DoWriteLog("SL_MediaPlayer","M_PeerConnection_RenegotiationNeeded");

                //Task.Run(() => CreateOffer());
            }
            catch (Exception Error)
            {
                DoWriteLog("SL_MediaClient", Error);
            }
        }

        private void M_PeerConnection_Connected()
        {
            try
            {
                DoWriteLog("SL_MediaPlayer", "M_PeerConnection_Connected");
            }
            catch (Exception Error)
            {
                DoWriteLog("SL_MediaClient", Error);
            }
        }

        private async void PeerConnection_LocalSdpReadytoSendAsync(SdpMessage message)
        {
            try
            {
                DoWriteLog("SL_MediaPlayer", "PeerConnection_LocalSdpReadytoSendAsync " + "_Channel" + " : " + Channel.ToString() + " " + " , " + "_BackendId" + " : " + BackendId.ToString());

                var url = await SendSdpAsync(message);
            }
            catch (Exception Error)
            {
                DoWriteLog("SL_MediaClient", Error);
            }
        }

        private async Task<Uri> SendSdpAsync(SdpMessage message)
        {
            SignalingRestMessage signalingRestMessage = new SignalingRestMessage()
            {
                backendId = BackendId,
                channelId = Channel,
                type = "video"
            };

            signalingRestMessage.SetOffer(message);

            var payload = JsonConvert.SerializeObject(signalingRestMessage, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None
            });

            HttpContent httpContent = new StringContent(payload, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await m_HttpClient.PostAsync("v1/webrtc/offer", httpContent);

            response.EnsureSuccessStatusCode();

            if (response.IsSuccessStatusCode)
            {
                string sourceString = await response.Content.ReadAsStringAsync();

                byte[] bytesBase64 = Convert.FromBase64String(sourceString.Replace("\"", "").Replace("\n", ""));
                string decodedString = Encoding.UTF8.GetString(bytesBase64);

                OfferRestReturn offerRestReturn = JsonConvert.DeserializeObject<OfferRestReturn>(decodedString);

                SdpMessage sdpMessage = new SdpMessage();
                sdpMessage.Type = SdpMessageType.Answer;
                sdpMessage.Content = offerRestReturn.sdp;

                await m_PeerConnection.SetRemoteDescriptionAsync(sdpMessage);
            }

            //string dataresponseAsString = Convert.ToBase64String(dataresponse);

            // return URI of the created resource.
            return response.Headers.Location;
        }

        private void PeerConnection_VideoTrackAdded(RemoteVideoTrack track)
        {
            track.Argb32VideoFrameReady += Track_Argb32VideoFrameReady;
        }

        private void Track_Argb32VideoFrameReady(Argb32VideoFrame frame)
        {
            try
            {
                if(ActionFrameReceive != null)
                {
                    ActionFrameReceive(frame.height, frame.width, frame.data, frame.stride);
                }
            }
            catch (Exception Error)
            {
                DoWriteLog("SL_MediaClient", Error);
            }

            System.Diagnostics.Debug.WriteLine("Track_Argb32VideoFrameReady");
        }

        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).

                    if (m_PeerConnection != null)
                    {
                        m_PeerConnection.VideoTrackAdded -= PeerConnection_VideoTrackAdded;
                        m_PeerConnection.LocalSdpReadytoSend -= PeerConnection_LocalSdpReadytoSendAsync;
                        m_PeerConnection.Close();
                        m_PeerConnection.Dispose();
                        m_PeerConnection = null;
                    }

                    if (m_HttpClient != null)
                    {
                        m_HttpClient.Dispose();
                        m_HttpClient = null;
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~framereceiverUWP() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    class SignalingRestMessage
    {
        public string offer { get; set; }
        public string backendId { get; set; }
        public uint channelId { get; set; }
        public string type { get; set; }

        public void SetOffer(SdpMessage message)
        {
            var offerRestMessage = new OfferRestMessage();

            offerRestMessage.type = "offer";
            offerRestMessage.sdp = message.Content;

            offer = JsonConvert.SerializeObject(offerRestMessage, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None
            });
        }
    }

    class OfferRestMessage
    {
        public string type { get; set; }
        public string sdp { get; set; }
    }

    class OfferRestReturn
    {
        public string type { get; set; }
        public string sdp { get; set; }
    }

    class videoFrame
    {
        public uint width;
        public uint height;
        public IntPtr data;
        public int stride;
    }
}
